#!/bin/sh
set -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

mkdir $DIR
tar -xf $3 --strip-components=1 -C $DIR
rm $3
XZ_OPT=--best tar -c -v -J -f $TAR \
    --exclude '*.jar' \
    --exclude 'gradle/wrapper' \
    --exclude 'gradlew*' \
    $DIR
rm -Rf $DIR
